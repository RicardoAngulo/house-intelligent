package com.example.ricardoangulo.upsin.mainActivity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ricardoangulo.upsin.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_BLUETOOTH = 1;
    @BindView(R.id.focusOneImageButton)
    ImageButton focusOneImageButton;
    @BindView(R.id.focusTwoImageButton)
    ImageButton focusTwoImageButton;
    @BindView(R.id.focusThreeImageButton)
    ImageButton focusThreeImageButton;
    @BindView(R.id.focusFourImageButton)
    ImageButton focusFourImageButton;
    @BindView(R.id.focusFiveImageButton)
    ImageButton focusFiveImageButton;
    @BindView(R.id.focusSixImageButton)
    ImageButton focusSixImageButton;
    @BindView(R.id.focusSevenImageButton)
    ImageButton focusSevenImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        checkPermission();
        bluetoothProvider();
    }

    @OnClick(R.id.focusOneImageButton) public void switchOnFocusOne(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus one 1", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusTwoImageButton) public void switchOnFocusTwo(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus two 2", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusThreeImageButton) public void switchOnFocusThree(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus three 3", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusFourImageButton) public void switchOnFocusFour(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus four 4", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusFiveImageButton) public void switchOnFocusFive(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus five 5", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusSixImageButton) public void switchOnFocusSix(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus six 6", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.focusSevenImageButton) public void switchOnFocusSeven(View view) {
        Toast.makeText(getBaseContext(), "Switch on the focus seven 7", Toast.LENGTH_LONG).show();
    }

    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.INTERNET
        }, 101);
    }

    public void bluetoothProvider() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent intentEnabledBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intentEnabledBluetooth, REQUEST_BLUETOOTH);
        }
    }
}
